# TP Atelier Django

## Plan

### Atelier Django 1 - Créer une application en Django

* Présentation (5')
* L'environnement de développement (20')
* Créer notre projet (10')
* Ma première page (15')
* Mon premier modèle (15')
* Vue liste et détail (20')
* Formulaire d'ajout (20')
* Relier deux modèles (20')

### Atelier Django 2 - Créer une API avec Django Rest Framework

### Atelier 3 - Créer une application VueJS

## Usage

Utilise `reaveal-md` pour générer des supports en revealjs à partir du markdown.

Installer les dépendances

    !console
    npm install

*Tip : utiliser [nvm](https://github.com/creationix/nvm) pour installer une distribution node.js. et utiliser reaveal-md*

Démarrer le support dans un navigateur

    !console
    npm start
