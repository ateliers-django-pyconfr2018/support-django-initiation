# Initiation à Django

---

# Faisons les présentations

----

### Emmanuelle Helly – Makina Corpus

![Makina Corpus](./img/logo-makina-corpus.svg)

Experts en logiciels libres, cartographie et analyse de données, nous concevons des applications métiers innovantes.

----

### Pierre Charlet – Hashbang

![Hashbang](./img/logo-hashbang.svg)

Hashbang aide ses clients à concevoir, réaliser, et exploiter des services web innovants.

---

# Sommaire

* Introduction
* L'environnement de développement
* Créer notre projet
* Ma première page
* Mon premier modèle
* L'administration Django
* Vues liste et détail
* Formulaire d'ajout
* Relier deux modèles

----

# Tutoriel fil rouge

Application de gestion de ressources pédagogiques

* Ressource : Titre, description, lien ou fichier, auteur, licence
* Auteur : Nom, email

---

# Introduction

----

## Django ?

* Framework Open Source en python pour le web
* Version courante : 2.1.2
* Communauté nombreuse
* Philosophie _keep it simple, stupid_ et _don't repeat yourself_
* Architecture _Model_, _Template_, _View_

----

## Architecture _Model_, _Template_, _View_ ?

* __Model__ : définition des objets. Django fournit un ORM pour accéder à la base de données
* __Template__ : affichage des données
* __View__ : fonction ou classes retournant des réponses HTTP

L'_URL dispatcher_ permet de faire correspondre des URLs sous forme d'expressions régulières à des vues.

Notes:

Questions comparaison avec le modèle MCV

---

# L'environnement de développement

----

## Installer et activer un _Virtualenv_

```bash
$ sudo apt install python3-venv
$ python3 -m venv venv
$ source venv/bin/activate # Active l'environnement virtuel
```

## Installation de Django

```bash
(venv) $ pip install django
```

Notes:

Préciser la version de python : python3 (pour avoir Django 2.1)

----

## Installer un _IDE_

Installer [PyCharm](https://www.jetbrains.com/help/pycharm/install-and-set-up-pycharm.html#linux)

---

# Créer notre projet

----

## Créer le projet

```bash
(venv) $ django-admin startproject monprojet
```

## Lancer le serveur

```bash
(venv) $ cd monprojet
(venv) $ ./manage.py runserver
```

Et accéder à [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

----

### It works !

![](./img/00-run-server.png)

----

## Créer l'application

```bash
(venv) $ ./manage.py startapp teaching
```

----

## Structure du projet

```bash
monprojet
├── db.sqlite3
├── manage.py
├── monprojet
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── teaching
    ├── admin.py
    ├── apps.py
    ├── migrations
    ├── models.py
    ├── tests.py
    └── views.py
```

----

## Projet vs. Application


### Une application

Constitue une fonctionnalité (système de blog, application de sondage)

### Un projet

Contient les réglages et les applications pour un site Web particulier

**Un projet est une combinaison d'applications**

---

# Quelques réglages

----

## Activer l'application

```python
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    ...
    'teaching',
]
```

## Mettre en français par défaut

```python
LANGUAGE_CODE = 'fr-fr'
```

----

## Générer la structure de la base de données

```bash
$ ./manage.py migrate
```

_Remarque : par défaut Django utilise SQLite, en production PostgreSQL est plus indiqué._

---

# Ma première page

----

## Créer une vue page d'accueil

`home_view` reçoit la requête en paramètre, et renvoie une réponse.

```python
### teaching/views.py
from django.http import HttpResponse

def home_view(request):
    return HttpResponse("<h1>Bienvenue !</h1>")
```

----

## Ajouter une URL

Déclarer un chemin et le relier à la vue

```python
# monprojet/urls.py
from teaching import views

urlpatterns = [
    path('', views.home_view),
    # …
]
```

----

## Ma page d'accueil

Sur [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

---

# Mon premier modèle

----

## Le modèle ressource et ses champs

En anglais : `CourseMaterial`

* title → CharField
* description → TextField
* url → URLField

[Documentation sur les champs](https://docs.djangoproject.com/fr/2.1/ref/models/fields/)

----

## Déclarer le modèle

```python
### teaching/models.py
class CourseMaterial(models.Model):
    """Model for CourseMaterial"""

    title = models.CharField(max_length=100)
    description = models.TextField(
        max_length=500, blank=True, null=True
    )
    url = models.URLField(
        max_length=200, blank=True, null=True
    )
```

----

## Migrations de la base de données

* Générer le fichier de migration

```bash
$ ./manage.py makemigrations
```

* Appliquer les migrations à la base de données

```bash
$ ./manage.py migrate
```

Et maintenant ?

---

# L'administration Django

----

## Créer un super utilisateur

```bash
$ ./manage.py createsuperuser
```

Puis se connecter sur <http://127.0.0.1:8000/admin/>

----

## Déclarer le modèle dans l'admin

```python
### teaching/admin.py
from django.contrib import admin
from teaching.models import CourseMaterial

admin.site.register(CourseMaterial)
```
----

## Modèle dans l'interface admin

![](./img/03-admin.png)


Ajoutez vos CourseMaterial !

----

## Améliorer l'affichage des objets dans l'admin

```python
### teaching/models.py
class CourseMaterial(models.Model):

    # …

    def __str__(self):
        return self.title
```


---

# Vues liste et détail

----

## Vue liste

Nous utilisons les classes génériques de Django

La vue

```python
from django.views.generic import ListView
from teaching.models import CourseMaterial

class CourseMaterialList(ListView):
    """CourseMaterial list view"""
    model = CourseMaterial
```

----

## URL pour la liste

```python
### teaching/urls.py
from django.urls import path
from teaching import views
urlpatterns = [
    path('list',
        views.CourseMaterialList.as_view(),
        name='list'),
]
```

----

## Inclusion des URLs de chaque app dans le projet

Inclure les URLs de l'app `teaching` dans le projet

```python
# monprojet/urls.py
from django.conf.urls import include
urlpatterns = [
    # …
    path('teaching/', include(
            ('teaching.urls', 'teaching'),
            namespace='teaching')
        ),
    # …
]
```

----

Toutes les URLs de l'app `teaching` répondront à `/teaching/<chemin>`

Aller sur l'adresse <http://127.0.0.1:8000/teaching/list>, une erreur est affichée : il manque un élément

----

## Le template :)

Il manquait le template (= Gabarit en français)

```jinja
<!-- teaching/templates/teaching/coursematerial_list.html -->
{% if object_list %}
<dl>
    {% for course in object_list %}
    <li>{{ course }}
        <br><small>{{ course.description }}</small>
    </li>
    {% endfor %}
</dl>
{% else %}
<p>Aucune tâche !</p>
{% endif %}
```

*Redémarrer le serveur django*

Notes:

Nom et chemin par défaut du template

----

## Vue détail

La vue

```python
from django.views.generic import ListView, DetailView

class CourseMaterialDetail(DetailView):
    """CourseMaterial detail view"""
    model = CourseMaterial
```

Notes: À faire selon le temps restant

----

## URL pour la vue détail

En plus organisé qu'au début (le chemin pour accéder à la liste est renommé)

```python
### teaching/urls.py
urlpatterns = [
    path('coursematerial',
        views.CourseMaterialList.as_view(),
        name='coursematerial-list'
    ),
    path(
        'coursematerial/<int:pk>',
        views.CourseMaterialDetail.as_view(),
        name='coursematerial-detail'
    ),
]
```

`<int:pk>` sert à identifier l'objet à afficher

----

## Le template

```jinja
<!-- teaching/templates/teaching/coursematerial_detail.html -->
<h1>{{ coursematerial.title }}</h1>

<dl>
    <dt>Description</dt>
    <dd>{{ coursematerial.description }}</dd>
    <dt>URL</dt>
    <dd>{{ coursematerial.url }}</dd>
</dl>
```

---

# Formulaire d'ajout

----

## Déclarer le formulaire

```python
### teaching/forms.py
class CourseMaterialForm(forms.ModelForm):

    class Meta:
        model = CourseMaterial
        fields = ("title", "description", "url")
```

----

## La vue formulaire

```python
### teaching/views.py
from teaching.form import CourseMaterialForm

class CourseMaterialCreateView(CreateView):
    """CourseMaterial create view"""
    form_class = CourseMaterialForm
    template_name = 'teaching/coursematerial_form.html'
    success_url = '/teaching/coursematerial'
```

----

## Le template pour l'ajout

```jinja
<!-- teaching/templates/teaching/coursematerial_form.html -->
<form method="post">
    {% csrf_token %} {{ form.as_p }}
    <input type="submit" value="Submit" />
</form>
```

---

# Relier deux modèles

----

## Les champs de relations

* ``models.ForeignKey`` → relation de type __1-N__
* ``models.ManyToManyField`` → relation de type __N-N__
* ``models.OneToOneField`` → relation de type __1-1__

----

## Le modèle Author

Pour cet exemple, relier une ressource à son auteur.

Le modèle `Author`

* name → CharField
* email → EmailField

Dans le modèle `CourseMaterial`, ajouter un champs de type clé étrangère

* author → ForeignKey

Notes: Autre exemple possible, ajouter des thématiques

----

## Déclaration du modèle

Déclaration du modèle `Author`

```python
### teaching/models.py
class Author(models.Model):
    """Model for Author"""

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
```

----

## Champs de relation entre les modèles

Ajout du champs de type `ForeignKey`

```python
### teaching/models.py
class CourseMaterial(models.Model):
    # …
    author = models.ForeignKeyField(
        'Author', related_name='courses',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
```

----

## Migrations

Générer et appliquer les migrations

```bash
$ ./manage.py makemigrations
$ ./manage.py migrate
```

----

##  Interface d'administration

Déclarer le modèle dans l'admin

```python
### teaching/admin.py
admin.site.register(Author)
```

Visiter <http://127.0.0.1:8000/admin/>

Notes: si on a le temps, ajouter l'auteur dans le formulaire d'ajout.

---

## Récupérer les sources du projet

Sur gitlab.com [https://gitlab.com/ateliers-django-pyconfr2018/tp-atelier-django](https://gitlab.com/ateliers-django-pyconfr2018/tp-atelier-django)

----

## Aller plus loin

* [Documentation](https://docs.djangoproject.com/fr/2.1/)
* [Tutorials](https://docs.djangoproject.com/fr/2.1/intro/)
* [Django packages](https://djangopackages.org/) : extensions
* [L'origine du projet](https://framagit.org/numahell/upload-proto/blob/master/design/upload.pdf)

Et nous proposons des formations Django !

Notes: Cet atelier ne couvre pas tous les tutorials, il y en a d'autres

---

# Merci !
